## Installation

```sh
python --version
Python 3.9.5
```


Install libraries.

```sh
$ pip install -r requirements.txt
```

Migrate Database...

```sh
python manage.py migrate    
```

Create super user...

```sh
python manage.py createsuperuser    
```

To run .

```sh
127.0.0.1:8000
```

> Note: `http://localhost:8000/admin` from admin.


## License

MIT

**Free Software, Hell Yeah!**
