from django.db import models

# Create your models here.
class Table(models.Model):        
    score_from = models.DecimalField(max_digits=10, decimal_places=3)
    score = models.DecimalField(max_digits=10, decimal_places=3)
    date = models.DateField()

    def __str__(self):
        return str(self.id) + ".- " + str(self.date)