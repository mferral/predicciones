from rest_framework import generics
from .models import Table
from .serializers import TableSerializer


# Create your views here.
class TableList(generics.ListAPIView):
    pagination_class = None
    serializer_class = TableSerializer

    def get_queryset(self):
        queryset = Table.objects.all()
        return queryset


class TableGet(generics.RetrieveAPIView):    
    queryset = Table.objects.all()
    serializer_class = TableSerializer 


class TableRangeDatesList(generics.ListAPIView):
    pagination_class = None
    serializer_class = TableSerializer

    def get_queryset(self):
        queryset = Table.objects.filter(date__range=[self.kwargs['date_init'], self.kwargs['date_end']])
        return queryset
    