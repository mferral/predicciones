from django.urls import path, re_path

from . import views

urlpatterns = [
    path('table/', views.TableList.as_view()),    
    path('table/<int:pk>/', views.TableGet.as_view()),
    re_path(r'^table/range/(?P<date_init>\d{4}-\d{2}-\d{2})/(?P<date_end>\d{4}-\d{2}-\d{2})/$', views.TableRangeDatesList.as_view()),
]